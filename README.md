LibreOffice Templates
---------------------

This repository contains my highly unofficial templates for LibreOffice, for use at ESS. Use at your own risk, no quality guaranteed.

### Installation

The installation instructions are as follows:

```
mkdir ~/Templates
git clone https://gitlab01.esss.lu.se/yngvelevinsen/libreoffice-templates.git ~/Templates/ess-templates/
```

This creates the folder *Templates* in your home directory (if it does not exist yet), and downloads the templates to a subfolder.

After this they should automatically show up in LibreOffice when you create a new document,
unless you have specified a different default path for your customized templates.

### Examples

#### Impress

![title page](ScreenShots/TitlePage.png)

![slide](ScreenShots/Slide.png)


### Contributors/Thanks to

 * Yngve Levinsen
 * Ryoichi Miyamoto

